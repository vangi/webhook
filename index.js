var express    = require('express');
var bodyParser = require('body-parser');
var request    = require('request');

const TOKEN_API = 'ijMMJQzAs5riz1SJAZrC4qin4q2BHDAt5CcRrsmgjcw6sYgGC4iIPCEoDPD0zzIP'; 

var app = express();
app.use(bodyParser.json());

app.listen(3099, function() {
    console.log('Estamos Activo en el puerto 3099');
});

app.get('/', function(req, res) {
    res.send('Bem vindos ao palestra')
});

app.post('/conversao', function(req, res) {
    var data = req.body;
    data.leads.forEach(function(pageEntry) {
        var form = {
            lead: pageEntry.id,
            email: pageEntry.email,
            nome: pageEntry.name,
            telefone: pageEntry.personal_phone,
            first_lead: pageEntry.first_conversion.content.identificador,
            last_lead: pageEntry.last_conversion.content.identificador,
            json: data,
            tipo: 'conversão',
            ref: 'rdStation'
        }
        request({
            method: 'POST',
            url: 'http://localhost:3098/api/leads?access_token=' + TOKEN_API,
            json: form
        });
    })
    res.sendStatus(200);
});

app.post('/oportunidade', function(req, res) {
    var data = req.body;
    data.leads.forEach(function(pageEntry) {
        var form = {
            lead: pageEntry.id,
            email: pageEntry.email,
            nome: pageEntry.name,
            telefone: pageEntry.personal_phone,
            first_lead: pageEntry.first_conversion.content.identificador,
            last_lead: pageEntry.last_conversion.content.identificador,
            json: data,
            tipo: 'oportunidade',
            ref: 'rdStation'
        }
        request({
            method: 'POST', 
            url: 'http://localhost:3098/api/leads?access_token=' + TOKEN_API,
            json: form
        });
    })
    res.sendStatus(200);
});

